IP Forward Test

Node.js API Server Enterprise Reporting Portal.
Requirements

Node.js: http://nodejs.org
Bunyan logger: npm install -g bunyan
Getting Started

Clone the repo
Install dependencies with npm install
Run the app with npm start and go here: http://localhost:5050/
Testing

Run all tests: npm test
Run the coverage report: npm test-cov


Log into personal VM
Clone this repo or pull down the most recent changes
Install node dependencies: sudo npm install
Build tpkg from project root directory: npm run test-and-build

API Documentation
