const http = require('http');

const port = 1337;

http.createServer((req, res) => {
  console.log(req.connection.remoteAddress);
  console.log(req.headers);
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  res.end('Hello World\n');
}).listen(port, () => {
  console.log(`Server running at http://locahost:${port}/`);
});
